<?php
/**
@author 	ThiagoCI <thiago@thiagoci.com>
______________
|  DICIONARIO
|-------------
|+palavra
|-------------
|+getPalavra()
|+setPalavra()
|+sugestao()
|+definicao()
|+verificaPalavra()
|_____________
**/
class Dicionario
{
	public $palavra;

	function Dicionario(){
		require_once "./simple_html_dom.php";
	}

	public function setPalavra($palavra){
		$this->palavra = trim($palavra);
	}
	public function getPalavra(){
		return $this->palavra;
	}

	public function sugestao(){
		$u = file_get_html("https://www.dicio.com.br/pesquisa.php?q=".$this->getPalavra());
		@$r = $u->find("div[id=enchant]")[0]->plaintext;
		if(isset($r)){
			$r = str_replace("?", "",$r); //remover ?
			$r = explode("   ", $r);
			$vqd =  $r[0]; //voce quis dizer
			$p = $r[1];
			if (strpos(",", $p) === false) {
				$vp = explode(",", $p);
				#return $vp; //retorna array com sugestao de palavras
				$q = "";
				foreach ($vp as $k) {
					$q .= "<a href='javascript: ipt(`defina ".$k."`)'>".$k."</a>";
				}
				return "Voc&ecirc quis dizer: ".$q." ?";
			}
		}else{
			return false; 
		}
	}
	public function definicao(){
	    if (@$u = file_get_html("https://www.dicio.com.br/".$this->getPalavra())) {
	    	return "Defini&ccedil;&atilde;o de: <b>".utf8_encode($u->find("img[class=imagem-palavra]")[0]->alt)."</b><br>".utf8_encode($u->find("p[itemprop=description]")[0]->innertext);		
	    }else{
	    	return "Palavra n&atilde;o encontrada !";
	    }
	}


	public function verificaPalavra(){
		if ($this->sugestao() == false) {
			//buscar a definição da palavra
			return $this->definicao();
			
		}else{
			//retorna sugestão de palavra
			return $this->sugestao();
		}
	}
}